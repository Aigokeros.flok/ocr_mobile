package com.example.ocrread;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import static android.app.PendingIntent.getActivity;

public class MainActivity extends AppCompatActivity {

    ImageView im1;
    TextView tx1;
    Button bt1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    im1 = (ImageView)findViewById(R.id.imageView);
    tx1 = (TextView)findViewById(R.id.text1);
    bt1 = (Button)findViewById(R.id.button);

    bt1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getTextFromImage((View)findViewById(R.id.imageView));
        }
    });
    Log.i("======>","gg");
//    getTextFromImage((View)findViewById(R.id.imageView));

    }

    public void getTextFromImage(View v){

//        Image im = ContextCompat.getDrawable(getActivity(), R.drawable.p);
//        Resources rs = getApplicationContext().getResources();
//
//        final int resourceId = rs.getIdentifier("p", "drawable", getApplicationContext().getPackageName());
//        Log.i( "====>",""+rs.getDrawable(resourceId));
        Bitmap bm = BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.imagefor);

        TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();

        if(!textRecognizer.isOperational()){
            Toast.makeText(getApplicationContext(), "Could not get the Text",Toast.LENGTH_SHORT).show();
        }else{
            Frame frame = new Frame.Builder().setBitmap(bm).build();

            SparseArray<TextBlock> items = textRecognizer.detect(frame);

            StringBuilder sb = new StringBuilder();

            for(int i=0; i<items.size();++i){
                TextBlock myItem = items.valueAt(i);
                sb.append(myItem.getValue());
                sb.append("\n");
            }

            tx1.setText(sb.toString());
        }

        Log.i(">>","here");
    }
}
